import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import javax.swing.JFileChooser;
import javax.swing.JFrame;

public class App extends JFrame {
	static JFileChooser fc = new JFileChooser();
	
	public static void main(String[] args) throws IOException, InterruptedException {
		
        fc.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);

        int res = fc.showOpenDialog(null);
        
        if(res == JFileChooser.APPROVE_OPTION){
        	
        	
            File diretorio = fc.getSelectedFile();
            
    		DirReader dirMetrics= new DirReader();
    		dirMetrics.listFiles(diretorio);
    		
    		
    		List<VersionReader> lista = dirMetrics.lista;
    		Collections.sort(lista,Comparator.comparing(VersionReader::getVersion));
    		
    		fc.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);

	        int res2 = fc.showOpenDialog(null);
	        
	        if(res2 == JFileChooser.APPROVE_OPTION){
	        	
	        	File file = fc.getSelectedFile();
		
	        	String dir = file+"\\LOG.csv";
	        	
	        	
		FileWriter fw =new FileWriter(dir);
		BufferedWriter bw = new BufferedWriter(fw);
		   
		   bw.write("M�S,LOC,FUN��ES\n");
		    
   	    for(VersionReader v:lista) {
   	       
   	     bw.write(v.toString());
   	    }
   	    
   	 
     bw.close();
     
     Process pro = Runtime.getRuntime().exec("cmd.exe /c "+dir);
       pro.waitFor();
	   
    }
    
        }
	   
    }
	
}
