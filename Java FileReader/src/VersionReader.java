
public class VersionReader {
	
	private int loc;
	private int numberMethods;
	private int version;

	public int getVersion() {
		return version;
	}

	public void setVersion(int version) {
		
		this.version = version;
	}

	public int getLoc() {
		return loc;
	}

	public void setLoc(int loc) {
		this.loc = loc;
	}

	public int getNumberMethods() {
		return numberMethods;
	}

	public void setNumberMethods(int numberMethods) {
		this.numberMethods = numberMethods;
	}

	@Override
	public String toString() {
		return  ""+version +',' + loc + ',' + numberMethods+'\n';
	}
	
}
