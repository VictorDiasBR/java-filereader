import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.swing.JOptionPane;

public class DirReader {

	private Reader fileMetrics;
	private int locDir;
	private int numberMethodsDir;
	
	int count = 0 ;
	private VersionReader vr;
	public List<VersionReader>lista = new ArrayList();
	
	public Reader getFileMetrics() {
		return fileMetrics;
	}
	
	public void setFileMetrics(Reader fileMetrics) {
		this.fileMetrics = fileMetrics;
	}



	public int getLocDir() {
		return locDir;
	}



	public void setLocDir(int locDir) {
		this.locDir = locDir;
	}

	public int getNumberMethodsDir() {
		return numberMethodsDir;
	}

	public void setNumberMethodsDir(int numberMethodsDir) {
		this.numberMethodsDir = numberMethodsDir;
	}


	public void listFiles(File dir) throws IOException {
		vr = new VersionReader();
	    if(count>0) {
	    	
	    	int v=Integer.parseInt(dir.getName());
	    	vr.setVersion(v);
	    	System.out.print(" Version-"+dir.getName());
	    } 
	    locDir=0;
	    numberMethodsDir=0;
	   
		for(File f: dir.listFiles()) {
			
			if(f.isFile()&& f.getName().contains(".c")||f.isFile()&& f.getName().contains(".h")) { 
				
				fileMetrics =new Reader();
				fileMetrics.contabilizar(f.getAbsolutePath());
				locDir+=fileMetrics.getLoc();
				numberMethodsDir+=fileMetrics.getNumberMethods();
				
			}else {  
				count++;
				listFiles(f);
			}
			
			if(f.isDirectory()) {
				
				vr.setLoc(locDir);
				vr.setNumberMethods(numberMethodsDir);
				lista.add(vr);
				System.out.println(" |LOC-"+locDir+" |Number of Methods-"+numberMethodsDir);
			}
		}
	}
	
		
	
}
