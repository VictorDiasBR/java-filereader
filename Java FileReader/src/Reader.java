import java.io.BufferedReader;

import java.io.FileReader;
import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Reader {

	private int loc;
	private int numberMethods;
	
	public int getNumberMethods() {
		return numberMethods;
	}

	public void setNumberMethods(int numberMethods) {
		this.numberMethods = numberMethods;
	}

	public int getLoc() {
		return loc;
	}

	public void setLoc(int loc) {
		this.loc = loc;
	}

	public void contabilizar(String dir) throws IOException {
		
		FileReader file = new FileReader(dir);
		BufferedReader br = new BufferedReader(file);

		String line;

		Pattern p = Pattern.compile("(\\S)"); 
		
		while ((line = br.readLine()) != null) {
			
			Matcher m = p.matcher(line);
			boolean find = m.find();
			
			if (find == true) {
				loc++;
			}
		
			if (line.matches("^(static|void|int|List|int|Integer|double|Double|String|string|char|long|Long|boolean|float).*([A-z]|main).*[(].*([A-z]|void|.*).*[)]$") == true) {
				System.out.println(line);
				numberMethods++;
			}
			
		}
	}
	
}
